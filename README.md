# LazBlend: A translucent source editor for lazarus.

## What is LazBlend?
LazBlend is a plugin for the Lazarus IDE which makes the IDE's source editor translucent.
With that feature, you can see what is happening behind the source editor (e-mails, websites, background tasks and so on).
It works on every platform which supports alpha-blending.

## Compatibility
As said above, it is cross-platform.

It's tested on Lazarus 1.4.2 and Windows XP x86.

## Installation guide
Just install it as any Lazarus package.

## Usage guide
Look for the menu in the "Tools" section. You can adjust the source editor's opacity there.

Additionally, default keyboard shortcuts are set.

## :bug: reports

Feel free to report :bug: and to make pull requests.