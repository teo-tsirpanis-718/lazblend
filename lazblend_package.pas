{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit lazblend_package;

interface

uses
  RegisterLazBlend, LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('RegisterLazBlend', @RegisterLazBlend.Register);
end;

initialization
  RegisterPackage('lazblend_package', @Register);
end.
